# SQLite3 Bindings for Gambit-C (Scheme)

These bindings can be loaded at runtime or compiled into an executable.
In order to successfully build the bindings, one has to make sure that
the sqlite (shared) library is available.

It is used in my language server, which can be referred to at:
[zamba language server](https://gitlab.com/tomaha.gq/zamba-ls)

## caveats

Checking for errors is only done rudimentary.
There is no support for blobs, yet.
The bindings are almost exactly like the C API.
The "tests" are not actual tests, yet.

# LICENSE

Public Domain
