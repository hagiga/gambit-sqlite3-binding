(c-declare #<<C-END

#include <stdlib.h>
#include <sqlite3.h>

C-END
)

(c-define-type sqlite3-int64 (type "sqlite3_int64"))
(c-define-type sqlite3 (struct "sqlite3"))
(c-define-type sqlite3* (pointer sqlite3))
(c-define-type sqlite3-stmt (struct "sqlite3_stmt"))
(c-define-type sqlite3-stmt* (pointer sqlite3-stmt))

(define (sqlite3--open database-name #!rest flags)
  (define SQLITE_OPEN_READONLY         #x00000001) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_READWRITE        #x00000002) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_CREATE           #x00000004) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_DELETEONCLOSE    #x00000008) ;/* VFS only */
  (define SQLITE_OPEN_EXCLUSIVE        #x00000010) ;/* VFS only */
  (define SQLITE_OPEN_AUTOPROXY        #x00000020) ;/* VFS only */
  (define SQLITE_OPEN_URI              #x00000040) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_MEMORY           #x00000080) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_MAIN_DB          #x00000100) ;/* VFS only */
  (define SQLITE_OPEN_TEMP_DB          #x00000200) ;/* VFS only */
  (define SQLITE_OPEN_TRANSIENT_DB     #x00000400) ;/* VFS only */
  (define SQLITE_OPEN_MAIN_JOURNAL     #x00000800) ;/* VFS only */
  (define SQLITE_OPEN_TEMP_JOURNAL     #x00001000) ;/* VFS only */
  (define SQLITE_OPEN_SUBJOURNAL       #x00002000) ;/* VFS only */
  (define SQLITE_OPEN_SUPER_JOURNAL    #x00004000) ;/* VFS only */
  (define SQLITE_OPEN_NOMUTEX          #x00008000) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_FULLMUTEX        #x00010000) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_SHAREDCACHE      #x00020000) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_PRIVATECACHE     #x00040000) ;/* Ok for sqlite3_open_v2() */
  (define SQLITE_OPEN_WAL              #x00080000) ;/* VFS only */
  (define SQLITE_OPEN_NOFOLLOW         #x01000000) ;/* Ok for sqlite3_open_v2() */
  (let* ((db* ((c-lambda ()
                         (pointer sqlite3*) "___return((sqlite3**) malloc(sizeof(void*)));")))
         (flag (fold bitwise-ior 0 (if (pair? flags) flags
                                       (list
                                         SQLITE_OPEN_READWRITE
                                         SQLITE_OPEN_CREATE))))
         (res ((c-lambda ((pointer sqlite3*) nonnull-UTF-8-string int)
                         int
                         "___return(sqlite3_open_v2(___arg2, ___arg1, ___arg3, NULL)); ")
               db* database-name flag)))
    (if (= res 0) db* (raise (sqlite3--error-string res)))))


(define SQLITE_PREPARE_PERSISTENT              #x01)
(define SQLITE_PREPARE_NORMALIZE               #x02)
(define SQLITE_PREPARE_NO_VTAB                 #x04)

(define (sqlite3--prepare db* sql-statement #!rest flags)
  (let* ((dereference (c-lambda ((pointer sqlite3-stmt*)) sqlite3-stmt* "___return((sqlite3_stmt*)*___arg1);"))
         (stmt* ((c-lambda () (pointer sqlite3-stmt*) "___return((sqlite3_stmt **) malloc(sizeof(void*)));")))
         (res ((c-lambda ((pointer sqlite3*)
                          nonnull-UTF-8-string
                          int
                          unsigned-int32
                          (pointer sqlite3-stmt*))
                         int "___return(sqlite3_prepare_v3(*___arg1, ___arg2, ___arg3, ___arg4, ___arg5, NULL));")
               db*
               sql-statement
               (string-length sql-statement)
               (if (pair? flags)
                   (fold bitwise-ior 0 flags)
                   0)
               stmt*)))
    (if (= res 0)
      (dereference stmt*)
      (raise (string-append "error in sqlite3--prepare: " (number->string res) " " (sqlite3--error-string res))))))

(define sqlite3--step! #f)
(let (( SQLITE_OK           0 ) ; Successful result
      ( SQLITE_ERROR        1 ) ; Generic error
      ( SQLITE_INTERNAL     2 ) ; Internal logic error in SQLite
      ( SQLITE_PERM         3 ) ; Access permission denied
      ( SQLITE_ABORT        4 ) ; Callback routine requested an abort
      ( SQLITE_BUSY         5 ) ; The database file is locked
      ( SQLITE_LOCKED       6 ) ; A table in the database is locked
      ( SQLITE_NOMEM        7 ) ; A malloc() failed
      ( SQLITE_READONLY     8 ) ; Attempt to write a readonly database
      ( SQLITE_INTERRUPT    9 ) ; Operation terminated by sqlite3_interrupt()
      ( SQLITE_IOERR       10 ) ; Some kind of disk I/O error occurred
      ( SQLITE_CORRUPT     11 ) ; The database disk image is malformed
      ( SQLITE_NOTFOUND    12 ) ; Unknown opcode in sqlite3_file_control()
      ( SQLITE_FULL        13 ) ; Insertion failed because database is full
      ( SQLITE_CANTOPEN    14 ) ; Unable to open the database file
      ( SQLITE_PROTOCOL    15 ) ; Database lock protocol error
      ( SQLITE_EMPTY       16 ) ; Internal use only
      ( SQLITE_SCHEMA      17 ) ; The database schema changed
      ( SQLITE_TOOBIG      18 ) ; String or BLOB exceeds size limit
      ( SQLITE_CONSTRAINT  19 ) ; Abort due to constraint violation
      ( SQLITE_MISMATCH    20 ) ; Data type mismatch
      ( SQLITE_MISUSE      21 ) ; Library used incorrectly
      ( SQLITE_NOLFS       22 ) ; Uses OS features not supported on host
      ( SQLITE_AUTH        23 ) ; Authorization denied
      ( SQLITE_FORMAT      24 ) ; Not used
      ( SQLITE_RANGE       25 ) ; 2nd parameter to sqlite3_bind out of range
      ( SQLITE_NOTADB      26 ) ; File opened that is not a database file
      ( SQLITE_NOTICE      27 ) ; Notifications from sqlite3_log()
      ( SQLITE_WARNING     28 ) ; Warnings from sqlite3_log()
      ( SQLITE_ROW         100) ; sqlite3_step() has another row ready
      ( SQLITE_DONE        101) ; sqlite3_step() has finished executing
      )
  (set! sqlite3--step!
    (lambda (sql-stmt*)
      (let ((res ((c-lambda (sqlite3-stmt*)
                            int "___return(sqlite3_step(___arg1));")
                  sql-stmt*)))
        (cond
          ((= res SQLITE_DONE) #f) ;; TODO: reset
          ((= res SQLITE_ROW) #t) ;; TODO: reset
          ((and (> res 0) (< res 29)) (raise (string-append "sqlite3 couldn't step properly: " (sqlite3--error-string res))))
          (else (raise (sqlite3--error-string res))))))))

(define sqlite3--bind! #f)

(let ((sqlite3--parameter-count (c-lambda (sqlite3-stmt*) int "sqlite3_bind_parameter_count"))
      (sqlite3--bind-double     (c-lambda (sqlite3-stmt* int double) int "sqlite3_bind_double"))
      (sqlite3--bind-int        (c-lambda (sqlite3-stmt* int int) int "sqlite3_bind_int"))
      (sqlite3--bind-int64      (c-lambda (sqlite3-stmt* int sqlite3-int64) int "sqlite3_bind_int64"))
      (sqlite3--bind-null       (c-lambda (sqlite3-stmt* int) int "sqlite3_bind_null"))
      (sqlite3--bind-text       (c-lambda (sqlite3-stmt* int nonnull-UTF-8-string int) int "___return(sqlite3_bind_text(___arg1, ___arg2, ___arg3, ___arg4, SQLITE_TRANSIENT));")))
  (set!
    sqlite3--bind!
    (lambda (statement #!rest args)
      (if (= (length args) (sqlite3--parameter-count statement))
        (begin
          (fold (lambda (val i)
                  (cond
                    ((null? val)     (sqlite3--bind-null   statement i     ))
                    ((not val)       (sqlite3--bind-null   statement i     ))
                    ((string? val)   (sqlite3--bind-text   statement i val (string-length val)))
                    ((integer? val)
                     (if (> val 4294967295)
                       (sqlite3--bind-int64  statement i val)
                       (sqlite3--bind-int    statement i val)))
                    ((real? val)     (sqlite3--bind-double statement i (exact->inexact val)))
                    (else (error "invalid value-to-sql conversion")))
                  (+ i 1))
                1 args)
          #!void)
        (error "invalid bind invocation")))))

(define sqlite3--query #f)
(let ((sqlite3--column-count  (c-lambda (sqlite3-stmt*) int "sqlite3_column_count"))
      (sqlite3--column-type   (c-lambda (sqlite3-stmt* int) int "sqlite3_column_type"))
      (sqlite3--column-double (c-lambda (sqlite3-stmt* int) double "sqlite3_column_double"))
      (sqlite3--column-int64  (c-lambda (sqlite3-stmt* int) int "sqlite3_column_int64"))
      (sqlite3--column-text   (c-lambda (sqlite3-stmt* int) nonnull-UTF-8-string "___return((char *)sqlite3_column_text(___arg1, ___arg2));")))
  (set!
    sqlite3--query
    (lambda (stmt thunk)
      (letrec ((row-length (sqlite3--column-count stmt))
               (collect (lambda (vec i)
                          (if (< i row-length)
                            (begin
                              (vector-set! vec i
                                           (case (sqlite3--column-type    stmt i)
                                             ((1) (sqlite3--column-int64  stmt i))
                                             ((2) (sqlite3--column-double stmt i))
                                             ((3) (sqlite3--column-text   stmt i))
                                             ; no blobs yet
                                             ((5)                              #f)
                                             (else (error "invalid datatype returned by query"))))
                              (collect vec (+ i 1)))
                            (vector->list vec))))
               (iter (lambda ()
                       (if (sqlite3--step! stmt)
                         (begin
                           (thunk (collect (make-vector row-length #f) 0))
                           (iter))))))
        (iter)
        (sqlite3--reset! stmt)
        #!void))))

(define sqlite3--reset!               (c-lambda (sqlite3-stmt*) int "sqlite3_reset"))
(define sqlite3--finalize!            (c-lambda (sqlite3-stmt*) int "sqlite3_finalize"))
(define sqlite3--affected-rows        (c-lambda ((pointer sqlite3*)) int "___return(sqlite3_changes(*___arg1));"))
(define sqlite3--limit                (c-lambda ((pointer sqlite3*) int int) int "___return(sqlite3_limit(*___arg1, ___arg2, ___arg3));"))

(define sqlite3--error-string         (c-lambda (int) nonnull-UTF-8-string "___return((char *)sqlite3_errstr(___arg1));"))
(define sqlite3--close                (c-lambda (sqlite3*) int "sqlite3_close"))

(define (sqlite3--exec db sql-string)
  (let ((sql (sqlite3--prepare db sql-string)))
    (sqlite3--step! sql)
    (sqlite3--finalize! sql)))

;; utilities

(define (sqlite3--changes db)
  (let ((count 0))
    (sqlite3--query (sqlite3--prepare db "SELECT changes();") (lambda (x) (set! count (car x))))
    count))

(define (sqlite3--total-changes db)
  (let ((count 0))
    (sqlite3--query (sqlite3--prepare db "SELECT total_changes();") (lambda (x) (set! count (car x))))
    count))
