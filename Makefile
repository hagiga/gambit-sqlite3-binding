# buildfile for Gambit-C Scheme sqlite3 bindings.

GSC=gsc
GSC_OPTIONS=
CC_OPTIONS="-Wall "
LD_OPTIONS="-lsqlite3"

OPTIONS= $(GSC_OPTIONS) -cc-options $(CC_OPTIONS) -ld-options $(LD_OPTIONS)

all:
	$(GSC) $(OPTIONS) -o $(BIN_DIR)/sqlite3-binding.o1 src/sqlite3.scm
