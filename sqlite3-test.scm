;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(load "src/zamba")

(define db (sqlite3--open "file::memory:?cache=shared"))
(print "created inmemory database successfully.\n")

(sqlite3--exec
  db
  "CREATE TABLE IF NOT EXISTS customer (id number,
                                        name number);" )
(print "prepared sql-create statement.\n")

(define insert-stmt
  (sqlite3--prepare
    db "insert or replace into customer values
    (?, ?);"))

;; propergate your schema
(define statement-create-table
  "ATTACH DATABASE ':memory:?cache=shared' as java;
  CREATE TABLE java.constructor ;; THIS IS NOT WORKING, DUE TO REDUNDANT ARGUMENTS
    (ref number,  ;; ref to binding
    scope text,
    argument text ,
    argtype text,
    name text);
  CREATE TABLE java.interfaces
    (ref number,  ;; ref to binding
    name text);
  CREATE TABLE java.bindings
    (uri text,  ;; the file
    scope text,  ;; used for resolution
    name text,  ;; the identifier
    start integer,  ;; in bytes
    end integer,
    type number, ;; this is the type of binding
    ;; optionals
    generic number,
    superclass text,
    interfaces integer);")

(sqlite3--exec db "BEGIN")
(time
(let lp ((i 0))
  (sqlite3--bind! insert-stmt i (* i 4))
  (sqlite3--step! insert-stmt)
  (sqlite3--reset! insert-stmt)
  (if (< i 10000)
    (lp (+ i 1)))))
(sqlite3--exec db "COMMIT")
(sqlite3--finalize! insert-stmt)
(print "inserted values successfully.\n")

(define delete-stmt
  (sqlite3--prepare
    db
    "DELETE FROM customer;"))

(define query-stmt
  (sqlite3--prepare
    db
    "SELECT rowid, * FROM customer WHERE id = ?;"))

(sqlite3--bind! query-stmt 1001)

(sqlite3--query
  query-stmt
  (lambda (x)
    (let ((name (cadr x)))
      (print "name: ")
      (print name)
      (newline))))

(print "query without params successfully.\n")

(define query-stmt-2
  (sqlite3--prepare db "SELECT * FROM customer WHERE id = ?"))

(sqlite3--bind! query-stmt-2 1011)

(sqlite3--query
  query-stmt-2
  (lambda (x)
    (let ((name (cadr x)))
      (print "name: ")
      (print name)
      (newline))))
